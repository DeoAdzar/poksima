package com.example.proyek1deo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class AddData extends AppCompatActivity implements View.OnClickListener {
    public static final int REQUEST_CODE_STORAGE = 100;
    int eventID = 0;
    EditText etNama, etContent;
    Button btnSimpan;
    String fileName = "";
    String tempCatatan = "";
    Toolbar tb2;
    AlertDialog.Builder alert;
    AlertDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_data);
        tb2 = findViewById(R.id.toolbar2);
        setSupportActionBar(tb2);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        etNama = findViewById(R.id.et_nama_file);
        etContent = findViewById(R.id.et_isi);
        btnSimpan = findViewById(R.id.btnSimpan);
        btnSimpan.setOnClickListener(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            fileName = extras.getString("filename");
            etNama.setText(fileName);
            getSupportActionBar().setTitle("Ubah Catatan");
        } else {
            getSupportActionBar().setTitle("Tambah Catatan");
        }
        eventID = 1;
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkPermission()) {
                ReadFile();
            }
        } else {
            ReadFile();
        }
    }

    private void ReadFile() {
        String path = Environment.getExternalStorageDirectory().toString() + "/kominfo.proyek1";
        File file = new File(path, etNama.getText().toString());
        StringBuilder text = new StringBuilder();
        if (file.exists()) {

            try {
                file.mkdirs();
                FileInputStream fis =new FileInputStream(file);
                if (fis != null){
                    InputStreamReader isr = new InputStreamReader(fis);
                    BufferedReader br = new BufferedReader(isr);
                    String line = null;
                    while((line= br.readLine())!=null){
                    text.append(line);
                    }
                    fis.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            tempCatatan = text.toString();
            etContent.setText(text.toString());
        }
    }

    private void creatAndUpdate() {
        String path = Environment.getExternalStorageDirectory().toString() + "/kominfo.proyek1";
        File parent = new File(path);
        if (parent.exists()) {
            File file = new File(path, etNama.getText().toString());
            try {
                FileOutputStream outputStream = new FileOutputStream(file);
                outputStream.write(etContent.getText().toString().getBytes());
                outputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(AddData.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else {
            parent.mkdirs();
            File file = new File(path, etNama.getText().toString());
            try {
                FileOutputStream outputStream = new FileOutputStream(file);
                outputStream.write(etContent.getText().toString().getBytes());
                outputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(AddData.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
        dialog.dismiss();
        onBackPressed();
    }


    private boolean checkPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this
                        , new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_STORAGE);
                return false;
            }
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CODE_STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (eventID == 1) {
                        ReadFile();
                    } else {
                        dialogKonfirmasi();
                    }
                }
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSimpan:
                eventID = 2;
                if (!tempCatatan.equals(etContent.getText().toString())) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        if (checkPermission()) {
                            dialogKonfirmasi();

                        }
                    } else {
                        dialogKonfirmasi();
                    }
                }
                break;
        }
    }

    private void dialogKonfirmasi() {
        alert = new AlertDialog.Builder(this)
                .setTitle("Simpan Catatan")
                .setMessage("Apakah anda yakin ingin meyimpan catatan ini ?")
                .setCancelable(false)
                .setIcon(R.drawable.ic_baseline_warning_24)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        creatAndUpdate();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
        dialog = alert.create();
        dialog.show();

    }

    @Override
    public void onBackPressed() {
        if (!tempCatatan.equals(etContent.getText().toString())) {
            dialogKonfirmasi();
        }
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}