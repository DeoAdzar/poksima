package com.example.proyek1deo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    public static final int REQUEST_CODE_STORAGE = 100;
    ListView lv;
    Toolbar tb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lv = findViewById(R.id.lvCatatan);
        tb = findViewById(R.id.toolbar);
        setSupportActionBar(tb);
        getSupportActionBar().setTitle("Aplikasi Catatan Deo");
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int pos, long l) {
                Intent i = new Intent(MainActivity.this,AddData.class);


                Map<String, Object> data = (Map<String, Object>) parent.getAdapter().getItem(pos);
                i.putExtra("filename", data.get("name").toString());
                Toast.makeText(MainActivity.this, "You Clicked "+data.get("name"), Toast.LENGTH_SHORT).show();
                startActivity(i);
            }
        });
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int pos, long l) {
                Map<String, Object> data = (Map<String, Object>) parent.getAdapter().getItem(pos);
                TampilKonfirmasi(data.get("name").toString());
                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT >= 23){
            if (checkPermission()){
                getListFolder();
            }
        }else{
            getListFolder();
        }
    }

    private void getListFolder() {
        String path = Environment.getExternalStorageDirectory().toString()+"/kominfo.proyek1";
        File directory = new File(path);

        if (directory.exists()){
            File[] files = directory.listFiles();
            String[] filenames = new String[files.length];
            String[] dateCreated = new String[files.length];
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyy HH:mm:ss");
            ArrayList<Map<String,Object>> itemList = new ArrayList<Map<String,Object>>();

            for (int i = 0; i < files.length; i++) {
                filenames[i] = files[i].getName();
                Date lastModified = new Date(files[i].lastModified());
                dateCreated[i] = sdf.format(lastModified);
                Map<String, Object> listItemMap = new HashMap<>();
                listItemMap.put("name",filenames[i]);
                listItemMap.put("date",dateCreated[i]);
                itemList.add(listItemMap);
                
            }
            SimpleAdapter sa = new SimpleAdapter(this,itemList
                    , android.R.layout.simple_list_item_2,new String[]{"name","date"}
                    ,new int[]{android.R.id.text1,android.R.id.text2});
            lv.setAdapter(sa);
            sa.notifyDataSetChanged();
        }
    }

    private boolean checkPermission() {
        if (Build.VERSION.SDK_INT >= 23){
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED){
                return true;
            }else{
                ActivityCompat.requestPermissions(this
                        ,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},REQUEST_CODE_STORAGE);
                return false;
            }
        }else{
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case REQUEST_CODE_STORAGE:
                if (grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    getListFolder();
                }
                break;
        }
    }

    private void TampilKonfirmasi(String name) {
        new AlertDialog.Builder(this)
                .setTitle("Hapus Catatan ini?")
                .setMessage("Apakah anda yakin ingin menghapus "+name+" ?")
                .setIcon(R.drawable.ic_baseline_warning_24)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        hapusFile(name);
                    }
                })
                .setNegativeButton("No",null).show();
    }

    private void hapusFile(String name) {
        String path = Environment.getExternalStorageDirectory().toString()+"/kominfo.proyek1";
        File directory = new File(path,name);
        if (directory.exists()){
            directory.delete();
        }
        getListFolder();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.addItem:
                Intent i = new Intent(this,AddData.class);
                startActivity(i);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}